# Example of encapsulation

Encapsulation is about grouping functionality and data together in one place (a class).

Everything you need to do something is in the class.

You will expose a method that allows other classes to call your class.

The rest of the functionality can be hidden from other classes, making it more simple for them.

## Example

For example, you could have the functionality of "sending an email".

Implementing this functionality could involve several things, like:

* Sanitizing the email address to avoid header injection
* Calling an upstream provider (like Mailchimp, Sendgrid, Jetmail, etc) to send the mail
* Calling your CRM system to update it

Each of these functions would be handled in multiple methods within the class.

Your class would expose one method, for example "sendMail", that other classes could call.

Other parts of your program don't need to care about how email functionality is managed, it is all in your mail class.

In other words, the complexity of email functionality is encapsulated into your class.