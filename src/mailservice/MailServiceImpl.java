package mailservice;

/**
 * This basic class illustrates the 
 * concept of encapsulating sending email 
 *  
 */
public class MailServiceImpl {
	
	/**
	 * Other classes will use this method to send email.
	 * They don't know anything about how this functionality
	 * is achieved.
	 * 
	 * @param $to
	 * @param $content
	 */
	public void sendMail(String recipientAddress, String content) {
		// other classes don't need to care about email header injection attacks
		String safeEmail = sanitizeEmailAddress(recipientAddress);
				
		String htmlMessage = buildEmailTemplateFromContent(content);
		
		callProvider(safeEmail, htmlMessage);
		
		updateCRM(safeEmail);
	}
	
	// ----------- All the functionality below this is encapsulated ------------
	
	/**
	 * This method relates to the functionality of sending an email 
	 * and is encapsulated into the class.
	 * Notice that we've elected to mark it as "protected", we'll be 
	 * looking at what that means later.
	 * @param emailAddress
	 * @return
	 */
	protected String sanitizeEmailAddress(String emailAddress) {
		// TODO implement this method
		return "";
	}
	
	/**
	 * Build an HTML message for the email using a template
	 * @param content
	 * @return
	 */
	protected String buildEmailTemplateFromContent(String content) {
		// TODO implement this method
		return "";
	}
	
	/**
	 * Call the email provider with the address and the HTML message
	 * @param email
	 * @param content
	 */
	protected void callProvider(String email, String content) {
		// TODO implement this method
	}
	
	/**
	 * Update our internal CRM so we can track 
	 * customer interaction in one place
	 * @param emailAddress
	 */
	protected void updateCRM(String emailAddress) {
		// TODO implement this method
	}
	
}
